import Vue from 'vue'
import App from './App'
import Router from 'vue-router'
import * as VueGoogleMaps from 'vue2-google-maps'
import Vuelidate from 'vuelidate'
import AccountKit from 'vue-facebook-account-kit'
import routes from './router'
import store from './store/index'
import config from './constants'

// eslint-disable-next-line
import components from './utils/components'

Vue.config.productionTip = false
Vue.use(Router)
Vue.use(Vuelidate)
Vue.use(AccountKit)
Vue.use(VueGoogleMaps, {
  load: {
    key: config.googleMapsKey,
    libraries: config.googleMapsLibrary
  }
})

const router = new Router({
  routes,
  mode: 'history'
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.isLoggedIn) {
      next({
        name: 'SignIn',
        query: {
          loggedIn: 0
        }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  store: store,
  components: { App },
  template: '<App/>'
})

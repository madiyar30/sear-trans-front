import SignIn from '../views/auth/SignIn'
import Vehicles from '../views/vehicles/Vehicles'
import ViewCategory from '../views/vehicles/Category'
import Company from '../views/materials/Company'
import Material from '../views/materials/Material'
import ListMaterial from '../views/materials/List'
import ListVehicle from '../views/vehicles/List'
import ListPart from '../views/parts/List'

const routes = [
  {
    path: '/',
    name: 'SignIn',
    component: SignIn
  },
  {
    path: '/vehicle',
    name: 'Vehicles',
    component: Vehicles,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/vehicle/list',
    name: 'ListVehicle',
    component: ListVehicle,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/vehicle_category/:id',
    name: 'ViewCategory',
    component: ViewCategory,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/material',
    name: 'Material',
    component: Material,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/company/:id',
    name: 'Company',
    component: Company,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/material/list',
    name: 'ListMaterial',
    component: ListMaterial,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/list/part',
    name: 'ListPart',
    component: ListPart,
    meta: {
      requiresAuth: true
    }
  }
]

export default routes

import SecureLS from 'secure-ls'
import config from '../constants'

const ls = new SecureLS({
  encodingType: 'rc4',
  isCompression: false,
  encryptionSecret: config.frontWord
})

export function getStorageParsed (key) {
  let item = localStorage.getItem(key)
  if (item !== null) {
    return JSON.parse(item)
  } else {
    return null
  }
}

export function getStorageItem (key) {
  let item = localStorage.getItem(key)
  if (item !== null) {
    return item
  } else {
    return null
  }
}

export function getLSItem (key) {
  let item = ls.get(key)
  if (item !== '') {
    return item
  } else {
    return null
  }
}

export function setLSItem (key, item) {
  ls.set(key, item)
}

export function removeLSItem (key) {
  ls.remove(key)
}

export function removeLSItems () {
  ls.removeAll()
}

const config = {
  frontWord: '_sear_securely_key_word',
  serverUrl: 'http://46.36.218.84/',
  apiUrl: 'http://46.36.218.84/api',
  socketUrl: 'http://46.36.218.84:3000',
  googleMapsKey: 'AIzaSyCfCn9nNisevOTwltHC5C_P9sU8fAAwa4c',
  googleMapsLibrary: 'places',
  mapOptions: {
    disableDefaultUI: true,
    panControl: false,
    zoomControl: true,
    fullscreenControl: false,
    mapTypeControl: false,
    scaleControl: false,
    overviewMapControl: false
  }
}
export default config

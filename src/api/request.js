import axios from 'axios'
import config from '../constants'

axios.defaults.baseURL = config.apiUrl

export function checkPhone (phone) {
  return new Promise((resolve, reject) => {
    axios.post('login', {
      phone: phone
    })
      .then(function () {
        resolve(true)
      })
      .catch(function (error) {
        reject(error)
      })
  })
}

export function accountKitCallback (credentials) {
  return new Promise((resolve, reject) => {
    if (credentials.response.status === 'PARTIALLY_AUTHENTICATED') {
      let code = credentials.response.code
      axios.get('https://graph.accountkit.com/v1.1/access_token?grant_type=authorization_code&code=' + code + '&access_token=AA|581141829029199|1ad05fdd8511cc11137789704220c578')
        .then(response => {
          axios.get('https://graph.accountkit.com/v1.3/me/?access_token=' + response.data.access_token)
            .then(response => {
              if (response.data.phone.national_number === credentials.phone) {
                resolve('success')
              } else {
                let error = 'Введите свой номер'
                reject(error)
              }
            })
            .catch(error => {
              reject(error)
            })
        })
        .catch(error => {
          reject(error.response)
        })
    } else if (credentials.response.status === 'NOT_AUTHENTICATED') {
      let error = 'Повторите попытку'
      reject(error)
    } else if (credentials.response.status === 'BAD_PARAMS') {
      let error = 'Повторите попытку'
      reject(error)
    }
  })
}

export function loginByPhone (phone) {
  return new Promise((resolve, reject) => {
    axios.post('login', {
      phone: phone
    })
      .then(response => {
        if (response.status === 200) {
          resolve(response.data.result)
        } else {
          resolve(response)
        }
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function auth (credentials) {
  return new Promise((resolve, reject) => {
    let data = []
    axios.post('auth', data, {
      headers: credentials
    })
      .then(response => {
        resolve(response.data)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function list (credentials) {
  return new Promise((resolve, reject) => {
    axios.get('list/' + credentials.url, {
      params: credentials.params
    })
      .then(response => {
        if (response.status === 200) {
          resolve(response.data)
        } else {
          resolve(response.data)
        }
      })
      .catch(error => {
        reject(error)
      })
  })
}

export function generalRequest (credentials) {
  return new Promise((resolve, reject) => {
    if (credentials.method === 'GET') {
      axios.get(credentials.url, {
        headers: credentials.headers,
        params: credentials.params
      })
        .then(response => {
          resolve(response.data)
        })
        .catch(error => {
          reject(error.response)
        })
    } else if (credentials.method === 'POST') {
      axios.post(credentials.url, credentials.data, {
        headers: credentials.headers
      })
        .then(response => {
          resolve(response.data)
        })
        .catch(error => {
          reject(error.response)
        })
    } else {
      let error = {
        status: 'not defined'
      }
      reject(error)
    }
  })
}

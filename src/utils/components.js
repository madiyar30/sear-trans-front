import Vue from 'vue'
import Header from '../components/main/Header'
import BigHeader from '../components/main/BigHeader'
import Footer from '../components/main/Footer'
import Map from '../components/main/Map'
import OrderDirect from '../components/vehicles/OrderDirect'
import Order from '../components/vehicles/Order'
import ListVehicle from '../components/vehicles/ListVehicle'
import ListItem from '../components/materials/ListItem'
import MaterialOrder from '../components/materials/MaterialOrder'
import LeftBar from '../components/main/LeftBar'

const components = [
  Vue.component('sear-header', Header),
  Vue.component('sear-big-header', BigHeader),
  Vue.component('sear-footer', Footer),
  Vue.component('sear-map', Map),
  Vue.component('sear-list-vehicle', ListVehicle),
  Vue.component('sear-order-direct', OrderDirect),
  Vue.component('sear-list-item', ListItem),
  Vue.component('sear-order', Order),
  Vue.component('sear-material-order', MaterialOrder),
  Vue.component('sear-left-bar', LeftBar)
]
export default components

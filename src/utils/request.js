import axios from 'axios'
import config from '../constants'

const service = axios.create({
  baseURL: config.apiUrl,
  timeout: 5000
})
// request interceptor
service.interceptors.request.use(
  (serviceConfig) => {
    return serviceConfig
  },
  (error) => {
    Promise.reject(error)
  }
)

// respone interceptor
service.interceptors.response.use(
  (response) => {
    const res = response.data
    return res
  },
  (error) => {
    return Promise.resolve(error)
  }
)
export default service

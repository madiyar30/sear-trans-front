import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import general from './modules/main'
import list from './modules/list'
import sockets from './modules/sockets'
import getters from './getters'

Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    user,
    sockets,
    general,
    list
  },
  getters
})

export default store

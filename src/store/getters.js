const getters = {
  userToken: state => state.user.token,
  userInfo: state => state.user.info,
  userPosition: state => state.user.position,
  isLoggedIn: state => state.user.token !== '',
  isSocketConnected: state => state.sockets.isConnected,
  getCommissions: state => state.list.commissions,
  getComputes: state => state.list.computes,
  getCities: state => state.list.cities,
  getMarks: state => state.list.marks,
  getTechs: state => state.list.techs,
  getRates: state => state.list.rates,
  getTransports: state => state.list.transports,
  getStatusGMap: state => state.general.status,
  getVehiclesGeo: state => state.list.vehicles,
  getMaterials: state => state.list.materials
}
export default getters

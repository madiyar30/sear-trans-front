import { loginByPhone, auth, checkPhone, accountKitCallback } from '../../../api/request'
import { getLSItem, setLSItem, removeLSItems } from '../../../helpers'

const user = {
  state: {
    token: getLSItem('_sear_key') || '',
    info: getLSItem('_sear_user') || null,
    position: getLSItem('_sear_user_position') || null
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    DELETE_TOKEN: (state) => {
      state.token = ''
      removeLSItems()
      localStorage.clear()
    },
    SET_INFO: (state, info) => {
      state.info = info
    },
    DELETE_INFO: (state) => {
      state.info = null
    },
    SET_POSITION: (state, position) => {
      state.position = position
    },
    DELETE_POSITION: (state) => {
      state.POSITION = null
    }
  },
  actions: {
    async checkPhone ({ commit }, credentials) {
      try {
        return await checkPhone(credentials.phone)
      } catch (e) {
        throw e
      }
    },
    async accountKitCallback ({ commit }, credentials) {
      try {
        return await accountKitCallback(credentials)
      } catch (e) {
        throw e
      }
    },
    async loginByPhone ({ commit }, userInfo) {
      try {
        const user = await loginByPhone(userInfo)
        const token = user.token
        delete user.token
        commit('SET_INFO', user)
        commit('SET_TOKEN', token)
        let now = new Date()
        let time = parseInt(now.setDate(now.getDate() + 1) / 1000).toString()
        setLSItem('_sear_key', token)
        setLSItem('_sear_expires', time)
        setLSItem('_sear_user', user)
        return user
      } catch (e) {
        return 404
      }
    },
    setUserPosition ({ commit }, position) {
      try {
        setLSItem('_sear_user_position', position)
        commit('SET_POSITION', position)
      } catch (e) {
        throw e
      }
    },
    expireTime ({ commit }) {
      try {
        let expires = parseInt(getLSItem('_sear_expires')) || null
        if (expires !== null) {
          let now = new Date()
          let nowInSeconds = parseInt(now.getTime() / 1000)
          if (nowInSeconds >= expires) {
            commit('DELETE_TOKEN')
            commit('DELETE_INFO')
          }
        }
      } catch (e) {
        throw e
      }
    },
    checkToken ({ commit }, token) {
      try {
        auth(token)
          .catch(error => {
            console.log(error)
            commit('DELETE_TOKEN')
          })
      } catch (e) {
        throw e
      }
    },
    logout ({ commit }) {
      try {
        commit('DELETE_TOKEN')
        commit('DELETE_INFO')
      } catch (e) {
        throw e
      }
    }
  }
}

export default user

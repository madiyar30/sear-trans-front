import { list, generalRequest } from '../../../api/request'
import { getLSItem, setLSItem, removeLSItem } from '../../../helpers'
const lists = {
  state: {
    commissions: getLSItem('_sear_list_commissions') || [],
    computes: getLSItem('_sear_list_computes') || [],
    cities: getLSItem('_sear_list_cities') || [],
    marks: getLSItem('_sear_list_marks') || [],
    techs: getLSItem('_sear_list_techs') || [],
    rates: getLSItem('_sear_list_rates') || {},
    transports: getLSItem('_sear_transports') || [],
    materials: getLSItem('_sear_materials') || [],
    materialsMap: getLSItem('_sear_materials_map') || [],
    vehicles: []
  },

  mutations: {
    SET_COMMISSIONS: (state, commissions) => {
      state.commissions = commissions
      setLSItem('_sear_list_commissions', commissions)
    },
    SET_COMPUTES: (state, computes) => {
      state.computes = computes
      setLSItem('_sear_list_computes', computes)
    },
    SET_CITIES: (state, cities) => {
      state.cities = cities
      setLSItem('_sear_list_cities', cities)
    },
    SET_MARKS: (state, marks) => {
      state.marks = marks
      setLSItem('_sear_list_marks', marks)
    },
    SET_TECHS: (state, techs) => {
      state.techs = techs
      setLSItem('_sear_list_techs', techs)
    },
    SET_RATES: (state, rates) => {
      state.rates = rates
      setLSItem('_sear_list_rates', rates)
    },
    SET_TRANSPORTS: (state, credentials) => {
      state.transports[credentials.tech_id] = []
      state.transports[credentials.tech_id] = credentials.data
      removeLSItem('_sear_transports')
      setLSItem('_sear_transports', state.transports)
    },
    SET_VEHICLES: (state, vehicles) => {
      state.vehicles = vehicles
    },
    SET_MATERIALS: (state, materials) => {
      setLSItem('_sear_materials', state.materials)
      state.materials = materials
    },
    SET_MATERIALS_MAP: (state, materials) => {
      state.materialsMap = materials
    }
  },

  actions: {
    async listCommissions ({ commit, state }) {
      try {
        if (state.commissions.length === 0) {
          let response = await list({
            url: 'commissions',
            params: {}
          })
          commit('SET_COMMISSIONS', response.result)
          return response.result
        }
      } catch (e) {
        throw e
      }
    },
    async listComputes ({ commit, state }) {
      try {
        if (state.computes.length === 0) {
          let response = await list({
            url: 'computes',
            params: {}
          })
          commit('SET_COMPUTES', response.result)
        }
      } catch (e) {
        throw e
      }
    },
    async listCities ({ commit, state }) {
      try {
        if (state.cities.length === 0) {
          let response = await list({
            url: 'cities',
            params: {}
          })
          commit('SET_CITIES', response.result)
        }
      } catch (e) {
        throw e
      }
    },
    async listMarks ({ commit, state }) {
      try {
        if (state.marks.length === 0) {
          let response = await list({
            url: 'marks',
            params: {}
          })
          commit('SET_MARKS', response.result)
        }
      } catch (e) {
        throw e
      }
    },
    async listTechs ({ commit, state }) {
      try {
        if (state.techs.length === 0) {
          let response = await list({
            url: 'techs',
            params: {}
          })
          commit('SET_TECHS', response.result)
        }
      } catch (e) {
        throw e
      }
    },
    async listRates ({ commit, state }) {
      try {
        if (state.rates.length === 0) {
          let response = await list({
            url: 'rates',
            params: {}
          })
          commit('SET_RATES', response.result)
        }
      } catch (e) {
        throw e
      }
    },
    async listTransport ({ commit, state }, credentials) {
      try {
        let response = await list({
          url: 'transports',
          params: {
            tech_id: credentials.tech_id
          }
        })
        commit('SET_TRANSPORTS', {
          data: response.result,
          tech_id: credentials.tech_id
        })
        return response.result
      } catch (e) {
        throw e
      }
    },
    async listMaterials ({ commit, state }) {
      try {
        let response = await list({
          url: 'material_cats'
        })
        commit('SET_MATERIALS', response.result)
        return response.result
      } catch (e) {
        throw e
      }
    },
    async listGeoPositions ({ commit, state }, credentials) {
      try {
        let response = await generalRequest({
          method: 'GET',
          url: 'geopositions',
          params: credentials.params,
          headers: credentials.headers
        })
        commit('SET_VEHICLES', response.result)
        return response.result
      } catch (e) {
        throw e
      }
    },
    async listMaterialsMap ({ commit, state }, credentials) {
      try {
        let response = await generalRequest({
          method: 'GET',
          url: 'material/companies',
          params: credentials.params,
          headers: credentials.headers
        })
        commit('SET_MATERIALS_MAP', response.result)
        return response.result
      } catch (e) {
        throw e
      }
    }
  }
}
export default lists

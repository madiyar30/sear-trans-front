import Vue from 'vue'
import config from '@/constants'
import VueSocketIO from 'vue-socket.io'

Vue.use(new VueSocketIO({
  debug: false,
  connection: config.socketUrl
}))
const vue = new Vue()
const socket = {
  state: {
    isConnected: false
  },

  mutations: {
    SOCKET_CONNECT (state) {
      state.isConnected = true
    },

    SOCKET_DISCONNECT (state) {
      state.isConnected = false
    },

    SOCKET_MESSAGECHANNEL (state, message) {
      state.socketStatus = message
    }
  },

  actions: {
    async SOCKET_CONNECT ({ commit }, userToken) {
      try {
        vue.$socket.emit('system', JSON.stringify(userToken))
        vue.sockets.subscribe('system', (data) => {
          let response = JSON.parse(data)
          if (response.statusCode === 200) {
            commit('SOCKET_CONNECT')
          } else {
            console.log('SOCKET: server error')
          }
        })
      } catch (e) {
        throw e
      }
    },
    async SOCKET_DISCONNECT ({ commit }, userToken) {
      try {
        vue.$socket.emit('disconnect', JSON.stringify(userToken))
        vue.sockets.subscribe('disconnect', (data) => {
          let response = JSON.parse(data)
          if (response.statusCode === 200) {
            commit('SOCKET_DISCONNECT')
          } else {
            console.log('SOCKET: server error')
          }
        })
      } catch (e) {
        throw e
      }
    },
    async SOCKET_GEO ({ commit }, credentials) {
      try {
        vue.$socket.emit('geo', JSON.stringify(credentials))
      } catch (e) {
        throw e
      }
    },
    techOrders ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {
          vue.$socket.emit(credentials.channel, JSON.stringify(credentials.body))
          vue.sockets.subscribe(credentials.channel, (data) => {
            let response = JSON.parse(data)
            if (response.statusCode === 200) {
              resolve(response)
            } else {
              reject(response)
            }
          })
        })
      } catch (e) {
        throw e
      }
    },
    techOrderOne ({ commit }, credentials) {
      try {
        // return new Promise((resolve, reject) => {
        vue.sockets.subscribe('tech_order_1', (data) => {
          let response = JSON.parse(data)
          console.log(response)
          // if (response.statusCode === 200) {
          //   resolve(response)
          // } else {
          //   reject(response)
          // }
        })
        // })
      } catch (e) {
        throw e
      }
    },
    techOrderTwo ({ commit }, credentials) {
      try {
        // return new Promise((resolve, reject) => {
        vue.sockets.subscribe('tech_order_2', (data) => {
          let response = JSON.parse(data)
          console.log(response)
          // if (response.statusCode === 200) {
          //   resolve(response)
          // } else {
          //   reject(response)
          // }
        })
        // })
      } catch (e) {
        throw e
      }
    },
    techOrderThree ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {
          vue.$socket.emit('tech_order_3', JSON.stringify(credentials))
          vue.sockets.subscribe('tech_order_3', (data) => {
            let response = JSON.parse(data)
            if (response.statusCode === 200) {
              resolve(response)
            } else {
              reject(response)
            }
          })
        })
      } catch (e) {
        throw e
      }
    },
    techOrderFour ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderFive ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderSix ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderSeven ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderEight ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderNine ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderTen ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderEleven ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderTwelve ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderThirteen ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    },
    techOrderFourteen ({ commit }, credentials) {
      try {
        return new Promise((resolve, reject) => {})
      } catch (e) {
        throw e
      }
    }
  }
}
export default socket

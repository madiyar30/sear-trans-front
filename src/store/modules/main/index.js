import { generalRequest } from '../../../api/request'
const general = {
  state: {
    info: '',
    status: false
  },

  mutations: {
    SET_STATUS: (state, status) => {
      state.status = status
    }
  },

  actions: {
    async generalRequest ({ commit }, credentials) {
      try {
        return await generalRequest(credentials)
      } catch (e) {
        throw e
      }
    },
    async gMapsLoaded ({ commit }, credentials) {
      return new Promise((resolve, reject) => {
        try {
          commit('SET_STATUS', true)
          resolve(true)
        } catch (e) {
          reject(e)
        }
      })
    }
  }
}
export default general

import { generalRequest } from '../../../api/request'
const actions = {

  actions: {
    async generalRequest ({ commit }, credentials) {
      try {
        return await generalRequest(credentials)
      } catch (e) {
        throw e
      }
    }
  }
}
export default actions
